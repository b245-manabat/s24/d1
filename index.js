// [SECTION] Exponent Operator
	// before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);

	// ES6
		const secondNum = Math.pow(8,2);
		console.log(secondNum);

//  [SECTION] Template Literals
	// allows us to write strings without using concatenation operator (+);
	// greatly helps with code readability

	let name = "John";
		// before ES6
		let message = "Hello" + name + "! Welcome to programming.";
		console.log(message);

		// After ES6
			// uses backticks(``)

		message = `Hello ${name}! Welcome to programming.`;
		console.log(message);

		// Template literals allows us to perform operations

		const interestRate = 0.1;
		const principal = 1000;
		console.log(`The interest on your savings account is: ${interestRate*principal}`);

// [SECTION] Array Destructuring
	// allows us to unpack elements in an array into distinct variables
	// allows us to name array elements with variables instead of using index number
	/*
		Syntax:
			let/const [variableNameA,variableNameB, . . ] = arrayName;
	*/

	const fullName = ["Juan", "Dela", "Cruz"];

		// before ES6
		let firstName = fullName[0];
		let middleName = fullName[0];
		let lastName = fullName[0];
		console.log(`Hello ${firstName} ${middleName} ${lastName}!`);

		// after ES6 Updates

		const [fName,mName,lName] = fullName;

		console.log(fName);
		console.log(mName);
		console.log(lName);

		// mini-activity
			// array destructuring
		let array = [1,2,3,4,5];
			// destruct the array

		const [one,two,three,four,five] = array;

		console.log(one);
		console.log(two);
		console.log(three);
		console.log(four);
		console.log(five);

// [SECTION] Objects Destructring
	// allows us to npack properties of objects into distinct variables
	/*
		Syntax:
			let/const {
				propertyNameA,
				propertyNameB,
				. . .
			} = objectName;
	*/

		// before ES6
	const person = {
		givenName : "Jane",
		maidenName : "Dela",
		familyName : "Cruz"
	}

	let gName = person.givenName;
	let midName = person.maidenName;
	let famName = person.familyName;
	console.log(gName);
	console.log(midName);
	console.log(famName);

		// after ES6 update
	let {
		familyName,
		givenName,
		maidenName
	} = person;
	console.log("Object Destructuring after ES6");
	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

// [SECTION] Arrow Functions
	// compact alternative syntax to traditional functions

	const hello = () =>{
		console.log("Hello World!")
	}

	hello();

	// function expression
/*	const hello = function(){
		console.log("Hello World!")
	}*/

	// function declaration
	/*function hello(){
		console.log("Hello World!")
	}*/

// [SECTION] Implicit Return
	/*
		there are instances when you can omit return statement
		this works because wven without using return keyword
	*/

	// single line only
	const add = (x,y) => {
		return x+y;
	};

	console.log("Implicit Return: ");
	console.log(add(1,2));

	const subtract = (x,y) => x-y;
	console.log(subtract(10,5));

// [SSECTION] Defalt Function Argument Value
	const greet = (firstName = "firstName", lastName = "lastName") => {
		return `Good afternoon, ${firstName} ${lastName}`
	}

	console.log(greet("Loven"))

/*	function greet(firstName = "firstName", lastName = "lastName"){
		return `Good afternoon, ${firstName} ${lastName}`
		}

	console.log(greet());*/

// [SECTION] class-based object blueprints
	// allows us to create.instantiation of objects using classes blueprints
	// create class
		// constructor is a special method of a class for creating/initializaing an object of the class

	/*
		Syntax::
		class className{
			constructor(objectValeA, objectValueB, . . .){
				this.objectPropertyA = objectValueA;
				this.objectPropertyB = objectValueB
			}
		}
	*/

	class Car {
		constructor(brand,name,year) {
			this.carBrand = brand;
			this.carName = name;
			this.carYear = year;
		}
	}

	let car = new Car("Toyota", "Hilux-pickup", 2015);
	console.log(car);